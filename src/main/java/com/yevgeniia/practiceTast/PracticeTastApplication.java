package com.yevgeniia.practiceTast;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticeTastApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticeTastApplication.class, args);
	}

}
